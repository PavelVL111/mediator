public class ToggleComponent extends Component{

    public ToggleComponent(Mediator mediator) {
        super(mediator);
    }

    public void setToggle(boolean toggle){
        mediator.toggleField(toggle);
    }
}
