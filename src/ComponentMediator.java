public class ComponentMediator implements Mediator{
    private FieldComponent fieldComponent;
    private ToggleComponent toggleComponent;
    private SendButtonComponent sendButtonComponent;
    private String massage;

    @Override
    public void enterMassage(String massage) {
        this.massage = massage;
        System.out.println("Entered massage: " + this.massage);
    }

    @Override
    public void toggleField(boolean toggle) {
        fieldComponent.setAvailable(toggle);
        System.out.println("Set toggle: " + toggle);
    }

    @Override
    public void sendMassage() {
            sendButtonComponent.sendMassage();
    }

    @Override
    public String getMassage(){
        return this.massage;
    }

    public void setFieldComponent(FieldComponent fieldComponent) {
        this.fieldComponent = fieldComponent;
    }

    public void setToggleComponent(ToggleComponent toggleComponent) {
        this.toggleComponent = toggleComponent;
    }

    public void setSendButtonComponent(SendButtonComponent sendButtonComponent) {
        this.sendButtonComponent = sendButtonComponent;
    }

}
