public class Demo {
    public static void main(String[] args) {
        ComponentMediator mediator = new ComponentMediator();
        FieldComponent fieldComponent = new FieldComponent(mediator);
        ToggleComponent toggleComponent = new ToggleComponent(mediator);
        SendButtonComponent sendButtonComponent = new SendButtonComponent(mediator);

        mediator.setFieldComponent(fieldComponent);
        mediator.setToggleComponent(toggleComponent);
        mediator.setSendButtonComponent(sendButtonComponent);

        toggleComponent.setToggle(true);
        fieldComponent.enterMassage("new massage");
        sendButtonComponent.sendMassage();
    }
}
