public class SendButtonComponent extends Component{

    public SendButtonComponent(Mediator mediator) {
        super(mediator);
    }

    public void sendMassage(){
        System.out.println("Send massage: " + mediator.getMassage());
        mediator.enterMassage("");
    }
}
