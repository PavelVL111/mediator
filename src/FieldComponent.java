public class FieldComponent extends Component {
    private boolean isAvailable;

    public FieldComponent(Mediator mediator) {
        super(mediator);
    }

    public void enterMassage(String massage){
        if(isAvailable){
           this.mediator.enterMassage(massage);
        } else {
            System.out.println("Field isn't available");
        }
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }
}
