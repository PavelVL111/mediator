public interface Mediator {
    void enterMassage(String massage);
    void toggleField(boolean toggle);
    void sendMassage();
    String getMassage();
}
